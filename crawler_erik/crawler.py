import dota2api
import json
from time import sleep
api = dota2api.Initialise("1B9E8D8FCFE1FA1BFC6817C7DFBDFB75")


def crawl_history(accountid, length):
    matchlist = []
    while len(set(matchlist)) < length:
        if len(matchlist) == 0:
            try:
                hist = api.get_match_history(account_id=accountid, min_players = 10)
            except:
                sleep(30)
                print('sleepy')
                crawl_history(accountid, length)
        else:
            try:
                hist = api.get_match_history(account_id=accountid, start_at_match_id=matchlist[-1], min_players = 10)
            except:
                sleep(30)
                print('sleep')
                crawl_history(accountid, length)
        sleep(2)

        hist_obj = json.loads(hist.json)

        for i in hist_obj['matches']:
            matchlist.append(i['match_id'])
        print(len(matchlist))
    return matchlist


def crawl_players(x, playerlist = None):
    if playerlist is None:
        playerlist = []
    matches_to_crawl = []
    sleep(5)
    print(x)
    try:
        match = api.get_match_details(match_id=x)
        sleep(2)
        print('gogo')
        match_obj = json.loads(match.json)
        for j in match_obj['players']:
            print("c_p: ")
            print(len(playerlist))
            if len(playerlist) > 101:
                break
            if j['account_id'] not in playerlist:
                print(j['account_id'])
                check = check500(j['account_id'])
                if check['flag'] == 1:
                    playerlist.append(j['account_id'])
                    matches_to_crawl.extend(check['mlist'])

        file = open("matches.json", "a")
        file.write(match.json)
        file.write("\n")
        file.close()
    except:
        print('w8')
        sleep(60)
    playerlist = set(playerlist)
    print(len(playerlist))

    return {'matches':matches_to_crawl, 'players':playerlist}


def crawl_matches(x):
    try:
        match = api.get_match_details(match_id=x)
        print('gogo')
        file = open("matches.json", "a")
        file.write(match.json)
        file.write("\n")
        file.close()
        sleep(2)

    except:
        print('w8')
        sleep(5)
        crawl_matches(x)


def check500(accountid):
    mlist = []
    flag = 0
    num_matches = 0
    num_matches_new = 1
    print("test")
    while num_matches < num_matches_new:
        if len(mlist) == 0:
            try:
                hist = api.get_match_history(account_id=accountid)
            except:
                sleep(30)
                print('sleepy')
        else:
            try:
                hist = api.get_match_history(account_id=accountid, start_at_match_id=mlist[-1], min_players = 10)
            except:
                sleep(30)
                print('sleep')
        sleep(2)
        if hist is not None:
            hist_obj = json.loads(hist.json)

            for i in hist_obj['matches']:
                mlist.append(i['match_id'])
        num_matches = num_matches_new
        num_matches_new = len(list(set(mlist)))
        print("check500:")
        print(len(list(set(mlist))))
        if len(mlist) > 500:
            flag = 1
            break
    return {'flag':flag, 'mlist':set(mlist)}
